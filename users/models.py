# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.


class Profile(models.Model):
    """
    Profile extends the default django User model
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    national_id = models.CharField(
        'National ID', max_length=12, unique=True, required=True)
    phone = models.CharField(
        'Phone Number', max_length=10, required=False, unique=True)

    # def __unicode__(self):
    #     return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    """
    Creates a user profile after a user instance is created and saved
    """
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    """
    Saves the user profile instance after it is created
    """
    instance.profile.save()
