from rest_framework import serializers
from .models import User,Profile

class UserSerializer(serializers.ModelSerializer):
    """
    User Model Serializer
    """
    class Meta:
        model = Profile