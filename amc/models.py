# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class AMC(models.Model):
    """
    Describes the Asset Management Company details
    """
    amc_code = models.CharField(
        max_length=3, blank=False, verbose_name='AMC Code')
    name = models.CharField(max_length=100, unique=True)
    launch_date = models.DateField(
        auto_now=False, auto_now_add=False, blank=True, null=True)
    total_aum = models.FloatField(blank=True, null=True)  # in Rs c

# metadata
    address = models.CharField(max_length=500, blank=True)
    email = models.EmailField(max_length=50, blank=True)
    website = models.URLField(max_length=50, blank=True)
    phone = models.CharField(max_length=100, blank=True)
    fax = models.CharField(max_length=100, blank=True)


class FundCategory(models.Model):
    '''
    Stores mutual fund category details. you can classify funds as you like
    Like liquid, debt, large cap equity, small cap equity, tax saver (jargon: ELSS), etc
    '''
    name = models.CharField(max_length=100, unique=True, blank=True)
    num_funds = models.IntegerField(null=True)


class FundScheme(models.Model):
    '''
    Stores a mutual fund's details
    Like Mirae Asset Emerging Bluechip Fund, Axis Long Term Equity etc
    '''
    name = models.CharField(max_length=100, unique=True)
    fund_category = models.ForeignKey(
        FundCategory, on_delete=models.PROTECT, blank=True, null=True, related_name='fund_category_list')
    fund_house = models.ForeignKey(
        AMC, on_delete=models.PROTECT, blank=True, null=True, related_name='fund_house_list')

     # key details
    objective = models.CharField(max_length=1000, blank=True)
    aum = models.FloatField(blank=True, null=True)  # in Rs cr
    aum_date = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
